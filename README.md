# gerenciadorTarefas

# **Tema: Sistema de Gerenciamento de Tarefas Pessoais**

# **Descrição do projeto:**
O objetivo desse projeto é criar um sistema simples de gerenciamento de tarefas pessoais que permita aos usuários criar, listar, atualizar e excluir tarefas. O sistema é construído em Java e segue os princípios da Programação Orientada a Objetos, além de estar conectado ao banco de dados MySQL.

# **Funcionalidades:**
1. Cadastro de tarefas: Os usuários devem ser capazes de adicionar tarefas com um título, descrição e data de vencimento;
2. Listagem de tarefas: O sistema deve exibir uma lista de todas as tarefas cadastradas, incluindo informações como título, descrição e data de vencimento;
3. Atualização de tarefas: Os usuários devem poder editar as informações de uma tarefa, como o título, a descrição e a data de vencimento;
4. Exclusão de tarefas: Os usuários devem ser capazes de excluir tarefas que não são mais relevantes.
5. Data de vencimento: Implemente uma funcionalidade que destaque as tarefas que estão vencidas ou que estão próximas da data de vencimento;
6. Prioridades: Adicione a capacidade de atribuir prioridades às tarefas, como alta, média e baixa, e permita que os usuários classifiquem ou filtrem as tarefas com base em suas prioridades.

# **Funcionalidades adicionais:**
- Usar conceitos de classes e objetos para modelar tarefas no sistema;
- Implementar uma interface gráfica simples para interagir com o sistema;
- Armazenar as informações das tarefas em um banco de dados simples.

Este projeto é uma oportunidade para aplicar conceitos de Programação Orientada a Objetos, como encapsulamento, herança e polimorfismo, de uma maneira prática e útil. Além disso, ele pode ser estendido com funcionalidades adicionais, como notificações, exportação/importação de dados ou integração com um banco de dados.