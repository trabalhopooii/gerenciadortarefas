// Importação das classes necessárias

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.text.SimpleDateFormat;  // Importação para lidar com formatação de datas
import java.util.Date;

// Definição da classe Tarefa para representar uma tarefa (Falta: implementar a classe Tarefa)
class Tarefa {

    String titulo;
    String descricao;
    Date dataVencimento;
    String prioridade;

    // Construtor da classe Tarefa
    public Tarefa(String titulo, String descricao, Date dataVencimento, String prioridade) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.dataVencimento = dataVencimento;
        this.prioridade = prioridade;
    }

    // Métodos getters para obter os detalhes da tarefa (Falta: implementar os getters)
    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public String getPrioridade() {
        return prioridade;
    }
}

// Definição da interface GerenciadorTarefasInterface
interface GerenciadorTarefasInterface {

    void listarTarefas();     // Método para listar tarefas (Falta: Implementar a lógica para listar)

    void adicionarTarefa();   // Método para adicionar uma tarefa (Falta: Implementar a lógica para adicionar)

    void editarTarefa();      // Método para editar uma tarefa (Falta: Implementar a lógica para editar)

    void excluirTarefa();     // Método para excluir uma tarefa (Falta: Implementar a lógica para excluir)
}

// Classe GerenciadorTarefas que implementa a interface GerenciadorTarefasInterface
public class GerenciadorTarefas implements GerenciadorTarefasInterface {

    private static Scanner scanner = new Scanner(System.in);  // Scanner para entrada do usuário
    private static List<Tarefa> listaDeTarefas = new ArrayList<>();  // Lista de tarefas

    // Método principal da aplicação
    public static void main(String[] args) {
        GerenciadorTarefas gerenciador = new GerenciadorTarefas();
        gerenciador.executar();
    }

    // Método executar é o ponto de entrada do programa
    public void executar() {
        while (true) {
            // Exibir menu de opções
            System.out.println("\nMenu:");
            System.out.println("1. Listar Tarefas");
            System.out.println("2. Adicionar Tarefa");
            System.out.println("3. Editar Tarefa");
            System.out.println("4. Excluir Tarefa");
            System.out.println("5. Sair");
            System.out.print("Escolha uma opção: ");
            int escolha = scanner.nextInt();
            scanner.nextLine(); // Consumir a quebra de linha

            switch (escolha) {
                case 1:
                    listarTarefas();  // Chama o método para listar tarefas
                    break;
                case 2:
                    adicionarTarefa();  // Chama o método para adicionar uma tarefa
                    break;
                case 3:
                    editarTarefa();  // Chama o método para editar uma tarefa
                    break;
                case 4:
                    excluirTarefa();  // Chama o método para excluir uma tarefa
                    break;
                case 5:
                    System.exit(0);  // Sai do programa
                default:
                    System.out.println("Opção inválida. Tente novamente.");
            }
        }
    }

    @Override
    public void listarTarefas() {
        // Método para listar tarefas (Falta: Implementar a lógica para listar)
        System.out.println("\nLista de Tarefas:");

        for (int i = 0; i < listaDeTarefas.size(); i++) {
            Tarefa tarefa = listaDeTarefas.get(i);
            // Exibe detalhes da tarefa
            System.out.println("ID: " + i);
            System.out.println("Título: " + tarefa.getTitulo());
            System.out.println("Descrição: " + tarefa.getDescricao());
            System.out.println("Data de Vencimento: " + new SimpleDateFormat("dd/MM/yyyy").format(tarefa.getDataVencimento()));
            System.out.println("Prioridade: " + tarefa.getPrioridade());
            System.out.println("--------------------");
        }
    }

    @Override
    public void adicionarTarefa() {
        // Método para adicionar uma tarefa (Falta: Implementar a lógica para adicionar)
        System.out.println("\nAdicionar Tarefa:");
        System.out.print("Título da Tarefa: ");
        String titulo = scanner.nextLine();
        System.out.print("Descrição da Tarefa: ");
        String descricao = scanner.nextLine();
        System.out.print("Data de Vencimento (dd/MM/yyyy): ");
        String dataVencimentoStr = scanner.nextLine();
        System.out.print("Prioridade da Tarefa: ");
        String prioridade = scanner.nextLine();

        try {
            // Converte a data de vencimento fornecida para o formato Date (Falta: Implementar a conversão)
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date dataVencimento = dateFormat.parse(dataVencimentoStr);

            // Cria uma nova tarefa e a adiciona à lista (Falta: Implementar a criação e adição)
            Tarefa novaTarefa = new Tarefa(titulo, descricao, dataVencimento, prioridade);
            listaDeTarefas.add(novaTarefa);
            System.out.println("Tarefa adicionada com sucesso!");
        } catch (Exception e) {
            System.out.println("Erro ao adicionar a tarefa. Certifique-se de fornecer uma data válida.");
        }
    }

    @Override
    public void editarTarefa() {
        // Método para editar uma tarefa (Falta: Implementar a lógica para editar)
        System.out.println("\nEditar Tarefa:");
        listarTarefas();  // Listar tarefas para escolher a que deseja editar
        System.out.print("Escolha o ID da tarefa que deseja editar: ");
        int idEdicao = scanner.nextInt();
        scanner.nextLine(); // Consumir a quebra de linha

        if (idEdicao >= 0 && idEdicao < listaDeTarefas.size()) {
            Tarefa tarefaParaEditar = listaDeTarefas.get(idEdicao);

            // Solicitar novos detalhes da tarefa (Falta: Implementar a solicitação e a atualização)
            System.out.print("Novo Título da Tarefa: ");
            String novoTitulo = scanner.nextLine();
            System.out.print("Nova Descrição da Tarefa: ");
            String novaDescricao = scanner.nextLine();
            System.out.print("Nova Data de Vencimento (dd/MM/yyyy): ");
            String novaDataVencimentoStr = scanner.nextLine();
            System.out.print("Nova Prioridade da Tarefa: ");
            String novaPrioridade = scanner.nextLine();

            try {
                // Converte a nova data de vencimento fornecida para o formato Date (Falta: Implementar a conversão)
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date novaDataVencimento = dateFormat.parse(novaDataVencimentoStr);

                // Atualiza os detalhes da tarefa (Falta: Implementar a atualização)
                tarefaParaEditar.titulo = novoTitulo;
                tarefaParaEditar.descricao = novaDescricao;
                tarefaParaEditar.dataVencimento = novaDataVencimento;
                tarefaParaEditar.prioridade = novaPrioridade;

                System.out.println("Tarefa editada com sucesso!");
            } catch (Exception e) {
                System.out.println("Erro ao editar a tarefa. Certifique-se de fornecer uma data válida.");
            }
        } else {
            System.out.println("ID de tarefa inválido. Nenhuma tarefa foi editada.");
        }
    }

    @Override
    public void excluirTarefa() {
        // Método para excluir uma tarefa (Falta: Implementar a lógica para excluir)
        System.out.println("\nExcluir Tarefa:");
        listarTarefas();  // Listar tarefas para escolher a que deseja excluir
        System.out.print("Escolha o ID da tarefa que deseja excluir: ");
        int idExclusao = scanner.nextInt();
        scanner.nextLine(); // Consumir a quebra de linha

        if (idExclusao >= 0 && idExclusao < listaDeTarefas.size()) {
            Tarefa tarefaParaExcluir = listaDeTarefas.get(idExclusao);

            // Remover a tarefa da lista (Falta: Implementar a remoção)
            listaDeTarefas.remove(tarefaParaExcluir);
            System.out.println("Tarefa excluída com sucesso!");
        } else {
            System.out.println("ID de tarefa inválido. Nenhuma tarefa foi excluída.");
        }
    }
}
